#!/bin/sh

DEVUAN_DIR="/devuan-www/src"

# Only clone main repo if directory does not exist
if [ ! -f "${DEVUAN_DIR}/Gemfile" ]; then
	git clone --single-branch --branch "${DEVUAN_WWW_BRANCH}" "${DEVUAN_WWW_REPO}" "${DEVUAN_DIR}"
fi

cd "${DEVUAN_DIR}"

if [ ! -d "${DEVUAN_DIR}/.bundle" ]; then
	mkdir "${DEVUAN_DIR}/.bundle"
fi

tee "${DEVUAN_DIR}/.bundle/config" << EOF
---
BUNDLE_PATH: "${DEVUAN_DIR}/../vendor/bundle"
BUNDLE_DISABLE_SHARED_GEMS: "true"
EOF

if [ ! -f "${DEVUAN_DIR}/config.rb" ]; then
	cp "${DEVUAN_DIR}/config.rb.dist" "${DEVUAN_DIR}/config.rb"
fi

# Create output dir if needed
mkdir -p "${DEVUAN_DIR_OUT}"||true
# Remove old files if needed
if [ -d "${DEVUAN_DIR_OUT}/public" ]; then
	rm -r "${DEVUAN_DIR_OUT}/public"
fi
# Build and pipe log to output dir
bundle exec middleman build --verbose 2>&1 | tee "${DEVUAN_DIR_OUT}/build.log"
# Place result in output dir
mv public "${DEVUAN_DIR_OUT}"
